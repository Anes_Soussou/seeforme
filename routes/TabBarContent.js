/**
 * create a custom bottom tab bar
 *
 */
import React from 'react';
import { Platform } from 'react-native';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import HomeIcon from '../components/svg/HomeIcon';
import AppointmentIcon from '../components/svg/AppointmentIcon';
import DoctorsIcon from '../components/svg/DoctorsIcon';
import MedicalRecordsIcon from '../components/svg/MedicalRecordsIcon';
import MedicinesIcon from '../components/svg/MedicinesIcon';
import Colors from '../constants/Colors';

const TabBarContent = ({ state, descriptors, navigation }) => {
  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  return (
    <View style={styles.container}>
      {state.routes.map((route, index) => {
        // tab bar content
        const visibleTabs = ['Home', 'Appointments', 'Doctors', 'MedicalRecord', 'Medicines'];

        if (!visibleTabs.find((item) => item === route.name)) {
          return;
        }
        // extracting the options object from the descriptors object
        const { options } = descriptors[route.key];
        // retrieve the label for the tab bar
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;
        // navigate to a specific screen in the app when a tab is pressed
        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });
          // navigate to the specified route
          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name, { screen: route.name});
          }
        };
        // emit a 'tabLongPress' event and pass the route.key as the target when the tab is long-pressed
        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };
       /* uses a switch statement to determine which icon should be displayed
          for each tab */
        let IconComponent = null;
        const extra = {};
        switch (route.name) {
          case 'Home':
            IconComponent = HomeIcon;
            break;
          case 'Appointments':
            IconComponent = AppointmentIcon;
            break;
          case 'Doctors':
            IconComponent = DoctorsIcon;
            break;
          case 'MedicalRecord':
            IconComponent = MedicalRecordsIcon;
            break;
          case 'Medicines':
            IconComponent = MedicinesIcon;
            extra.fill = isFocused ? Colors.secondary : Colors.grey;
            extra.width = 23;
            break;
          case 'Account':
            IconComponent = HomeIcon;
            break;
          default:
            IconComponent = HomeIcon;

            break;
        }

        return (
          <View style={styles.tabBarItem} key={index}>
            <TouchableOpacity
              accessibilityRole="button"
              accessibilityState={isFocused ? { selected: true } : {}}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={onPress}
              onLongPress={onLongPress}
              style={styles.touchableOpacity}
            >
              <IconComponent
                width="25"
                height="25"
                stroke={isFocused ? Colors.secondary : Colors.grey}
                {...extra}
              />
              <Text
                numberOfLines={1}
                style={{
                  color: isFocused ? '#E4261B' : '#515151',
                  fontFamily: 'Metropolis',
                  fontSize: 12,
                  textAlign: 'center',
                  flex: 1,
                }}
              >
                {label}
              </Text>
            </TouchableOpacity>
          </View>
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    marginBottom: Platform.OS === 'ios' ? 30 : 10,
    backgroundColor: '#fff',
    height: 56,
  },
  tabBarItem: {
    flex: 1,
    marginHorizontal: 5,
    justifyContent: 'center',
  },
  touchableOpacity: {
    height: 44,
    paddingVertical: 3,
    alignItems: 'center',
  },
  image: {
    width: 24,
    height: 24,
    resizeMode: 'contain',
  },
});

export default TabBarContent;
