/**
 * The drawer navigation allows the user to navigate between differ routes
 */
import React, { useEffect, useRef } from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import TabNavigation from "./TabNavigation";
import DrawerContent from "./DrawerContent";

const Drawer = createDrawerNavigator();

const DrawerNavigation = (props) => {
  const responseListener = useRef();

  return (
    <Drawer.Navigator
      drawerContent={(props) => {
        return <DrawerContent {...props} />;
      }}
      initialRouteName="Home"
    >
      <Drawer.Screen name="Home" component={TabNavigation} />
      <Drawer.Screen name="Appointments" component={TabNavigation} />
      <Drawer.Screen name="Doctors" component={TabNavigation} />
      <Drawer.Screen name="MedicalRecord" component={TabNavigation} />
      <Drawer.Screen name="Medicines" component={TabNavigation} />
    </Drawer.Navigator>
  );
};

export default DrawerNavigation;
