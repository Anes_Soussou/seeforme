/*  define a navigation stack for the Doctors screen */
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Image, View } from 'react-native';
import { Icon } from '@rneui/themed';
import { Keyboard } from 'react-native';
import Colors from '../constants/Colors';
import Conversation from '../screens/Conversation';

const Stack = createStackNavigator();
const ConversationNavigation = (props) => (
  <Stack.Navigator>
    <Stack.Screen
      name="Conversation"
      component={Conversation}
      options={{
        title: 'Conversation',
        headerTitleAlign: 'center',
        headerStyle: {
          backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          //fontFamily: 'Metropolis-Bold',
        },
        headerTintColor: '#fff',
        headerStyle: {
          backgroundColor: Colors.primary,
        },
        headerLeft: () => (
          <Icon
            containerStyle={{ marginLeft: 20 }}
            type="ionicon"
            name="arrow-back-outline"
            color="#fff"
            onPress={() => {
              Keyboard.dismiss();
              props.navigation.goBack();
            }}
            size={23}
          />
        ),
        headerRight: () => (
          <View style={{ marginRight: 20 }}>
            <Image
              style={{ width: 35, height: 35 }}
              source={require('../assets/images/logo-min.png')}
              resizeMode="contain"
            />
          </View>
        ),
      }}
    />
  </Stack.Navigator>
);

export default ConversationNavigation;
