/*  define a navigation stack for the Appointments screen */
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Image, View } from 'react-native';
import { Icon } from '@rneui/themed';
import { Keyboard } from 'react-native';
import Colors from '../constants/Colors';
import MyAppointments from '../screens/MyAppointments';

const Stack = createStackNavigator();
const MyAppointmentsNavigation = (props) => (
  <Stack.Navigator>
    <Stack.Screen
      name="MyAppointments"
      component={MyAppointments}
      options={{
        title: 'MyAppointments',
        headerTitleAlign: 'center',
        headerStyle: {
          backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
        //  fontFamily: 'Metropolis-Bold',
        },
        headerTintColor: '#fff',
        headerStyle: {
          backgroundColor: Colors.primary,
        },
        headerLeft: () => (
          <Icon
            containerStyle={{ marginLeft: 20 }}
            type="ionicon"
            name="arrow-back-outline"
            color="#fff"
            onPress={() => {
              Keyboard.dismiss();
              props.navigation.goBack();
            }}
            size={23}
          />
        ),
      }}
    />
  </Stack.Navigator>
);

export default MyAppointmentsNavigation;
