/**
 * The DrawerContent component shows the user information, different
 * buttons and icons, and handle navigation.
 */
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { DrawerContentScrollView } from '@react-navigation/drawer';
import { Image } from 'react-native';
import Colors from '../constants/Colors';
import HomeIcon from '../components/svg/HomeIcon';
import SuiviniButton from '../components/ui/SuiviniButton';
import AppointmentIcon from '../components/svg/AppointmentIcon';
import DoctorsIcon from '../components/svg/DoctorsIcon';
import MedicalRecordsIcon from '../components/svg/MedicalRecordsIcon';
import MedicinesIcon from '../components/svg/MedicinesIcon';

const DrawerContent = (props) => {
  const { index, routes } = props.navigation.dangerouslyGetState();
  const route = routes[index];

  let routeName = route.name;

  if (route.state) {
    routeName = route.state.routeNames[route.state.index];
  }


  return (
    <View style={{ flex: 1 }}>
      <DrawerContentScrollView {...props}>
        <View style={styles.drawerContent}>
          <View style={styles.userInfoSection}>
            <View style={styles.avatarWrapper}>
              <Image
                style={styles.avatar}
                source={require('../assets/avatar.png')}
              />
            </View>
              <View>
                <Text style={styles.title}>{'SUVIVNI'}</Text>
                <Text style={styles.caption}>{'Chayma Guirat'}</Text>
              </View>
          </View>

          <View style={styles.drawerSection}>
            <View style={{ backgroundColor: 'red' }}></View>

            <SuiviniButton
              text={'Home'}
              iconLeft={() => (
                <HomeIcon style={{ marginRight: 14 }} height="45" width="24" stroke="#FFF" />
              )}
              textStyle={styles.buttonTextStyle}
              style={{
                ...styles.button,
                backgroundColor: routeName === 'Home' ? Colors.secondary : Colors.primary,
                paddingLeft: 20,
                justifyContent: 'flex-start',
              }}
              onPress={() => {
                props.navigation.navigate('Home', { screen: 'Home' });
              }}
            />
              <SuiviniButton
                text={'Appointments'}
                iconLeft={() => (
                  <AppointmentIcon style={{ marginRight: 14 }} height="55" width="24" stroke="#FFF" />
                )}
                textStyle={styles.buttonTextStyle}
                style={{
                  ...styles.button,
                  backgroundColor: routeName === 'Appointments' ? Colors.secondary : Colors.primary,
                  paddingLeft: 20,
                  justifyContent: 'flex-start',
                }}
                onPress={() => {
                  props.navigation.navigate('Home', { screen: 'Appointments' });
                }}
              />
                <SuiviniButton
                  text={'Doctors'}
                  iconLeft={() => (
                    <DoctorsIcon style={{ marginRight: 14 }} height="30" width="24" stroke="#FFF" />
                  )}
                  textStyle={styles.buttonTextStyle}
                  style={{
                    ...styles.button,
                    backgroundColor: routeName === 'Doctors' ? Colors.secondary : Colors.primary,
                    paddingLeft: 20,
                    justifyContent: 'flex-start',
                  }}
                  onPress={() => {
                    props.navigation.navigate('Home', { screen: 'Doctors' });
                  }}
                />
              <SuiviniButton
                text={i18n.t('MedicalRecord')}
                iconLeft={() => (
                  <MedicalRecordsIcon style={{ marginRight: 14 }} height="30" width="24" stroke="#FFF" />
                )}
                textStyle={styles.buttonTextStyle}
                style={{
                  ...styles.button,
                  backgroundColor: routeName === 'MedicalRecord' ? Colors.secondary : Colors.primary,
                  paddingLeft: 20,
                  justifyContent: 'flex-start',
                }}
                onPress={() => {
                  props.navigation.navigate('Home', { screen: 'MedicalRecord' });
                }}
              />
              <SuiviniButton
                text={i18n.t('Medicines')}
                iconLeft={() => (
                  <MedicinesIcon style={{ marginRight: 14 }} height="30" width="24" stroke="#FFF" />
                )}
                textStyle={styles.buttonTextStyle}
                style={{
                  ...styles.button,
                  backgroundColor: routeName === 'Medicines' ? Colors.secondary : Colors.primary,
                  paddingLeft: 20,
                  justifyContent: 'flex-start',
                }}
                onPress={() => {
                  props.navigation.navigate('Home', { screen: 'Medicines' });
                }}
              />
          </View>
        </View>
      </DrawerContentScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
  },
  userInfoSection: {
    paddingTop: 50,
    paddingBottom: 40,
    paddingHorizontal: 40,
    flexDirection: 'row',
    alignItems: 'center',
  },
  avatarWrapper: {
    marginRight: 20,
  },
  avatar: {
    width: 43,
    height: 43,
  },
  title: {
    fontSize: 15,
    color: Colors.primary,
    fontFamily: 'Metropolis-Bold',
  },
  caption: {
    fontSize: 10,
    color: Colors.primary,
    fontFamily: 'Metropolis',
  },
  drawerSection: {
    paddingHorizontal: 20,
  },
  bottomDrawerSection: {
    paddingHorizontal: 20,
  },
  button: {
    backgroundColor: Colors.primary,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingLeft: 40,
    height: 57,
  },
  buttonTextStyle: {
    fontSize: 17,
    textTransform: 'none',
  },
});


export default DrawerContent;
