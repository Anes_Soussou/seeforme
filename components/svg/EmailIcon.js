import React from "react";
import Svg, {Path } from "react-native-svg";

function EmailIcon(props) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width ? props.width : "20.972"}
      height={props.height ? props.height :"13.981"}
      viewBox="0 0 20.972 13.981"
      style={props.style ? props.style : {}} 
    >
      <Path
        fill={props.fill ? props.fill : "#28367a"}
        d="M4.5 9v13.981h20.972V9zm10.486 7.641L6.095 9.874h17.782zm-9.612 5.466V10.425l6.286 4.784-3.719 4.222.11.11 4.309-3.8 2.626 2 2.627-2 4.309 3.8.11-.11-3.719-4.231 6.287-4.775v11.682z"
        transform="translate(-4.5 -9)"
      ></Path>
    </Svg>
  );
}

export default EmailIcon;
