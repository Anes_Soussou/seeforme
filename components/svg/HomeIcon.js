import React from "react";
import Svg, { Path } from "react-native-svg"
import PropTypes from 'prop-types';


function HomeIcon(props) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width ? props.width : "21.504"}
      height={props.height ? props.height : "21.763"}
      viewBox="0 0 21.504 21.763"
      style={props.style? props.style : {}}


    >
      <Path
        fill="none"
        stroke={props.stroke ? props.stroke : "#000"}
        strokeWidth="1.1"
        d="M20.532 10.084l-9.316-9.066a.666.666 0 00-.931 0L.973 10.084a.674.674 0 00.465 1.156h2.337v9.1a.671.671 0 00.669.673h3.457v-6.925a.671.671 0 01.669-.673h4.364a.671.671 0 01.669.673v6.924h3.457a.671.671 0 00.669-.673v-9.1h2.337a.674.674 0 00.466-1.155z"
        data-name="Tracé 2263"
      ></Path>
    </Svg>
  );
}
HomeIcon.prototype = {
    height: PropTypes.number,
    width: PropTypes.number,
    fill: PropTypes.string,
    stroke: PropTypes.string,
  };
export default HomeIcon;