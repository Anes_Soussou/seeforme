import React from "react";
import Svg, {G, Path } from "react-native-svg";

function CalendarRDVIcon(props) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width ? props.width : "19.582"}
      height={props.height ? props.height : "21.535"}
      viewBox="0 0 19.582 21.535"
      style={props.style ? props.style : {}}
    >
      <G
        fill={props.fill ? props.fill : "none"}
        stroke={props.stroke ? props.stroke : "#28367a"}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={props.strokeWidth ? props.strokeWidth : "2"}
        data-name="Icon feather-calendar"
        transform="translate(-3.5 -2)"
      >
        <Path
          d="M6.454 4.954h13.674a1.954 1.954 0 011.954 1.954v13.674a1.954 1.954 0 01-1.954 1.954H6.454A1.954 1.954 0 014.5 20.582V6.908a1.954 1.954 0 011.954-1.954z"
          data-name="Tracé 2324"
        ></Path>
        <Path d="M17.198 3v3.907" data-name="Tracé 2325"></Path>
        <Path d="M9.384 3v3.907" data-name="Tracé 2326"></Path>
        <Path d="M4.5 10.814h17.582" data-name="Tracé 2327"></Path>
      </G>
    </Svg>
  );
}

export default CalendarRDVIcon;
