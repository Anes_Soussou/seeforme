import React from "react";
import Svg, {Path } from "react-native-svg";

function EditIcon(props) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width ?props.width : "13.893"}
      height={props.height ?props.height : "13.892"}
      viewBox="0 0 13.893 13.892"
      style={props.style ? props.style : {}}
    >
      <Path
        fill={props.fill ? props.fill :"none"}
        stroke={props.stroke ? props.stroke : "#000"}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
        d="M10.125 1.267a1.768 1.768 0 112.5 2.5l-8.442 8.438-3.437.938.938-3.437z"
        data-name="Tracé 2269"
      ></Path>
    </Svg>
  );
}

export default EditIcon;
