import React from "react";
import Svg, {G , Path } from "react-native-svg"
import PropTypes from 'prop-types';


function MedicalRecordsIcon(props) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={ props.width ?props.width : "17.664"}
      height={ props.height ?props.height : "23.485"}
      viewBox="0 0 17.664 23.485"
      style={props.style ? props.style : {}}
    >
      <G
        fill="#fff"
        stroke={props.stroke ? props.stroke : "#000"}
        strokeWidth="0.5"
        data-name="list (1)"
        transform="translate(.1 .1)"
      >
        <Path
          d="M17.464 0h-15.1v2.365H0v17.122l3.8 3.8h11.3V20.92h2.365zM3.638 22.353L.931 19.646h2.707zm10.915.386H4.184V19.1H.546V2.911h14.007zm2.365-2.365H15.1V2.365H2.911V.546h14.007z"
          data-name="Tracé 2222"
        ></Path>
        <Path d="M2.456 4.548h7.277v.546H2.456z" data-name="Tracé 2223"></Path>
        <Path d="M2.456 6.003h5.821v.546H2.456z" data-name="Tracé 2224"></Path>
        <Path
          d="M4.183 8.186h-2v2h2zm-.546 1.455h-.91v-.91h.91z"
          data-name="Tracé 2225"
        ></Path>
        <Path d="M5.366 9.641h5.821v.546H5.366z" data-name="Tracé 2226"></Path>
        <Path d="M5.366 8.186h7.277v.546H5.366z" data-name="Tracé 2227"></Path>
        <Path
          d="M4.183 11.824h-2v2h2zm-.546 1.455h-.91v-.91h.91z"
          data-name="Tracé 2228"
        ></Path>
        <Path d="M5.366 13.28h5.821v.546H5.366z" data-name="Tracé 2229"></Path>
        <Path d="M5.366 11.824h7.277v.546H5.366z" data-name="Tracé 2230"></Path>
        <Path
          d="M4.183 15.463h-2v2h2zm-.546 1.455h-.91v-.91h.91z"
          data-name="Tracé 2231"
        ></Path>
        <Path d="M5.366 16.918h5.821v.546H5.366z" data-name="Tracé 2232"></Path>
        <Path d="M5.366 15.463h7.277v.546H5.366z" data-name="Tracé 2233"></Path>
        <Path d="M5.366 20.556h5.821v.546H5.366z" data-name="Tracé 2234"></Path>
        <Path d="M5.366 19.101h7.277v.546H5.366z" data-name="Tracé 2235"></Path>
      </G>
    </Svg>
  );
}
MedicalRecordsIcon.prototype={
    height: PropTypes.number,
    width: PropTypes.number,
    fill: PropTypes.string,
    stroke: PropTypes.string,
}
export default MedicalRecordsIcon;
