import React from "react";
import Svg, { G ,Path } from "react-native-svg"
import PropTypes from 'prop-types';


function DoctorsIcon(props) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width ? props.width : "17.664 "}
      height={props.height ? props.height : "23.485"}
      viewBox="0 0 17.664 23.485"
      style={props.style? props.style : {}}

    >
      <G
        fill={props.fill ? props.fill : "#FFF"}
        stroke={props.stroke ? props.stroke : "#000"}
        strokeWidth={props.strokeWidth ? props.strokeWidth : "0.4"}
        transform="translate(.1 .1)"
      >
        <Path
          d="M17.464 0h-15.1v2.365H0v20.92h15.1V20.92h2.365zM.546 22.739V2.911h10.369v3.638h3.638v16.19zM11.461 3.3L14.167 6h-2.706zm5.457 17.078H15.1V6.163l-3.8-3.8H2.91V.546h14.008z"
          data-name="Tracé 2209"
        ></Path>
        <Path d="M2.456 5.276h7.277v.546H2.456z" data-name="Tracé 2210"></Path>
        <Path d="M2.456 6.731h5.821v.546H2.456z" data-name="Tracé 2211"></Path>
        <Path d="M6.458 8.914h2.365v.546H6.458z" data-name="Tracé 2212"></Path>
        <Path d="M6.458 10.369h2.365v.546H6.458z" data-name="Tracé 2213"></Path>
        <Path d="M2.456 13.28h6.367v.546H2.456z" data-name="Tracé 2214"></Path>
        <Path d="M2.456 16.19h6.367v.546H2.456z" data-name="Tracé 2215"></Path>
        <Path d="M6.458 11.824h2.365v.546H6.458z" data-name="Tracé 2216"></Path>
        <Path d="M2.456 17.646h6.367v.546H2.456z" data-name="Tracé 2217"></Path>
        <Path d="M2.456 19.101H7.55v.546H2.456z" data-name="Tracé 2218"></Path>
        <Path d="M2.456 14.735h6.367v.546H2.456z" data-name="Tracé 2219"></Path>
        <Path
          d="M12.189 9.641h-.546V8.55h-1.637v1.091H9.46v9.444l1.364 2.274 1.364-2.274v-8.171h.546v3.365h.546v-3.911h-1.092zm-1.637-.546h.546v.546h-.546zm1.091 1.091v.91h-1.637v-.91zm-1.637 8.55v-7.095h1.637v7.095zm.209.546h1.219l-.61 1.016z"
          data-name="Tracé 2220"
        ></Path>
        <Path
          d="M5.639 8.914H2.183v3.456h3.456zm-.546 2.911H2.728V9.46h2.365z"
          data-name="Tracé 2221"
        ></Path>
      </G>
    </Svg>
  );
}
DoctorsIcon.prototype={
    height: PropTypes.number,
    width: PropTypes.number,
    fill: PropTypes.string,
    stroke: PropTypes.string,
}
export default DoctorsIcon;
