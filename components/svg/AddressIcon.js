import React from 'react';
import Svg, { Path } from 'react-native-svg';

function AddressIcon(props) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width ? props.width : 17.694}
      height={props.height ? props.height : 23.092}
      viewBox="0 0 17.694 23.092"
      {...props}
    >
      <Path
        data-name="Icon awesome-map-marker-alt"
        d="M7.265 21.156C1.137 12.273 0 11.362 0 8.1a8.1 8.1 0 0116.194 0c0 3.265-1.137 4.176-7.265 13.059a1.013 1.013 0 01-1.664 0zm.835-9.685A3.374 3.374 0 104.723 8.1 3.374 3.374 0 008.1 11.471z"
        fill={props.fill ? props.fill : 'none'}
        stroke={props.stroke ? props.stroke : '#28367a'}
        strokeWidth={1.5}
        transform="translate(.75 .75)"
      />
    </Svg>
  );
}

export default AddressIcon;
