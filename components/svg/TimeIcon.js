import React from 'react';
import Svg, { Path } from 'react-native-svg';

function TimeIcon(props) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width ? props.width : 22.184}
      height={props.height ? props.height : 22.184}
      viewBox="0 0 22.184 22.184"
      {...props}
    >
      <Path
        data-name="Path 2328"
        d="M14.456 3.375a11.092 11.092 0 1011.1 11.092 11.088 11.088 0 00-11.1-11.092zm.011 19.965a8.873 8.873 0 118.873-8.873 8.873 8.873 0 01-8.873 8.873z"
        transform="translate(-3.375 -3.375)"
        fill={props.fill ? props.fill : '#28367a'}
      />
      <Path
        data-name="Path 2329"
        d="M18.2 10.688h-1.662v6.655l5.823 3.493.832-1.365-4.993-2.96z"
        transform="translate(-6.555 -5.142)"
        fill={props.fill ? props.fill : '#28367a'}
      />
    </Svg>
  );
}

export default TimeIcon;
