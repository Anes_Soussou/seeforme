import React from "react";
import { Text, View, ScrollView, StyleSheet, Image } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import Colors from "../constants/Colors";
import { SafeAreaView } from "react-native-safe-area-context";


const appointments = [
  {
    doctorName: "Dr. John Doe",
    specialty: "Dermatologist",
    date: "2023-05-10",
    time: "14:30",
  },
  {
    doctorName: "Dr. Khalfallah Mayssa",
    specialty: "Cardiologist",
    date: "2023-05-12",
    time: "09:00",
  },
  {
    doctorName: "Dr. Anissa Gharbi",
    specialty: "ENT",
    date: "2023-05-15",
    time: "16:45",
  },
  {
    doctorName: "Dr. John Doe",
    specialty: "Dermatologist",
    date: "2023-06-10",
    time: "10:30",
  },
  {
    doctorName: "Dr. John Doe",
    specialty: "Dermatologist",
    date: "2023-07-10",
    time: "10:30",
  },
];

const MyAppointments = function (props) {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.navbar}>
        <View style={styles.leftContainer}>
          <Ionicons name="menu" size={30} color="black" />
        </View>
        <View style={styles.centerContainer}>
          <Text style={styles.title}>Suivini</Text>
        </View>
        <View style={styles.rightContainer}>
          <Ionicons name="person-circle" size={30} color="black" />
        </View>
      </View>

      <ScrollView style={styles.appointmentsContainer}>
        {appointments.map((appointment, index) => (
          <View key={index} style={styles.appointment}>
            <View style={styles.appointmentLeft}>
              <Text style={styles.appointmentTitle}>{appointment.doctorName}</Text>
              <Text style={styles.appointmentSubtitle}>{appointment.specialty}</Text>
            </View>
            <View style={styles.appointmentRight}>
              <Text style={styles.appointmentDate}>{appointment.date}</Text>
              <Text style={styles.appointmentTime}>{appointment.time}</Text>
            </View>
          </View>
        ))}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#E7E7E7",
  },
  navbar: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 16,
    height: 60,
    backgroundColor: "#E7E7E7",
    marginTop: 10,
  },
  leftContainer: {
    flex: 1,
    alignItems: "flex-start",
  },
  centerContainer: {
    flex: 1,
    alignItems: "center",
  },
  rightContainer: {
    flex: 1,
    alignItems: "flex-end",
  },
  title: {
    fontSize: 23,
    fontWeight: "bold",
    color: Colors.med,
  },
  appointmentsContainer: {
    marginTop: 40,
  },
  appointment: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "white",
    marginHorizontal: 16,
    marginVertical: 8,
    paddingVertical: 12,
    paddingHorizontal: 16,
    borderRadius: 8,
    elevation: 2,
    borderColor: Colors.primary,
    borderWidth: 1,
  },
  appointmentLeft: {
    flex: 1,
    alignItems: "flex-start",
  },
  appointmentTitle: {
    fontWeight: "bold",
    fontSize: 16,
    color: Colors.primary,
  },
  appointmentSubtitle: {
    fontSize: 14,
    color: "black",
  },
});

export default MyAppointments;
