import React from "react";
import { Text, View, ScrollView, StyleSheet, Image } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import Colors from "../constants/Colors";
import { SafeAreaView } from "react-native-safe-area-context";

const medicalRecords = [
  {
    date: "2023-05-10",
    diagnosis: "Common cold",
    medications: ["Antihistamine", "Cough syrup"],
  },
  {
    date: "2023-05-15",
    diagnosis: "Allergic reaction",
    medications: ["Epinephrine", "Antihistamine"],
  },
  {
    date: "2023-05-20",
    diagnosis: "Sinusitis",
    medications: ["Antibiotics", "Nasal spray"],
  },
  {
    date: "2023-06-02",
    diagnosis: "Gastritis",
    medications: ["Proton pump inhibitor", "Antacids"],
  },
  {
    date: "2023-06-12",
    diagnosis: "Hypertension",
    medications: ["Beta blockers", "Diuretics"],
  },
  
];

const MedicalRecords = function (props) {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.navbar}>
        <View style={styles.leftContainer}>
          <Ionicons name="menu" size={30} color="black" />
        </View>
        <View style={styles.centerContainer}>
          <Text style={styles.title}>Suivini</Text>
        </View>
        <View style={styles.rightContainer}>
          <Ionicons name="person-circle" size={30} color="black" />
        </View>
      </View>

      <ScrollView style={styles.medicalRecordsContainer}>
        {medicalRecords.map((record, index) => (
          <View key={index} style={styles.medicalRecord}>
            <View style={styles.medicalRecordLeft}>
              <Text style={styles.medicalRecordDiagnosis}>
                {record.diagnosis}
              </Text>
              <Text style={styles.medicalRecordDate}>{record.date}</Text>
            </View>
            <View style={styles.medicalRecordRight}>
              {record.medications.map((medication, index) => (
                <Text key={index} style={styles.medication}>
                  {medication}
                </Text>
              ))}
            </View>
          </View>
        ))}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#E7E7E7",
  },
  navbar: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 16,
    height: 60,
    backgroundColor: "#E7E7E7",
    marginTop: 10,
  },
  leftContainer: {
    flex: 1,
    alignItems: "flex-start",
  },
  centerContainer: {
    flex: 1,
    alignItems: "center",
  },
  rightContainer: {
    flex: 1,
    alignItems: "flex-end",
  },
  title: {
    fontSize: 23,
    fontWeight: "bold",
    color: Colors.med,
  },
  medicalRecordsContainer: {
    marginTop: 40,
  },
  medicalRecord: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "white",
    marginHorizontal: 16,
    marginVertical: 8,
    paddingVertical: 12,
    paddingHorizontal: 16,
    borderRadius: 8,
    elevation: 2,
    borderColor: Colors.primary,
    borderWidth: 1,
  },
  medicalRecordLeft: {
    flex: 1,
    alignItems: "flex-start",
  },
  medicalRecordRight: {
    flex: 1,
    alignItems: "flex-end",
  },
  medicalRecordDate: {
    fontSize: 14,
    color: Colors.med,
  },
  medicalRecordDiagnosis: {
    fontSize: 16,
    color: "black",
    marginTop: 4,
    fontWeight: "bold",
  },
  medication: {
    fontSize: 14,
    color: Colors.primary,
    marginTop: 4,
    fontWeight: "bold",
  },
});

export default MedicalRecords;
