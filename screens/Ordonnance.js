import React, { useState } from "react";
import {
  Text,
  View,
  ScrollView,
  StyleSheet,
  Image,
  TextInput,
  Button,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import Colors from "../constants/Colors";
import { SafeAreaView } from "react-native-safe-area-context";
import { Alert } from "react-native";


const Ordonnance = function (props) {
  const [username, setUsername] = useState("");
  const [cin, setCin] = useState("");
  const [tel, setTel] = useState("");
  const [maladie, setMaladie] = useState("");
  const [medecines, setMedecines] = useState("");
  const [conseils, setConseils] = useState("");

  const handleUsernameChange = (value) => {
    setUsername(value);
  };

  const handleCinChange = (value) => {
    setCin(value);
  };

  const handleTelChange = (value) => {
    setTel(value);
  };

  const handleMaladieChange = (value) => {
    setMaladie(value);
  };

  const handleMedecinesChange = (value) => {
    setMedecines(value);
  };

  const handleConseilsChange = (value) => {
    setConseils(value);
  };

  const handleSubmit = () => {
    // Handle form submission here
  
      Alert.alert(
        "Prescription validated",
        "Your Prescription has been successfully submitted!"
      );

  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.navbar}>
        <View style={styles.leftContainer}>
          <Ionicons name="menu" size={30} color="black" />
        </View>
        <View style={styles.centerContainer}>
          <Text style={styles.title}>Suivini</Text>
        </View>
        <View style={styles.rightContainer}>
          <Ionicons name="person-circle" size={30} color="black" />
        </View>
      </View>
      <ScrollView>
        <View style={styles.form}>
          <Text style={styles.label}>Username</Text>
          <TextInput
            style={styles.input}
            placeholder="Enter your username"
            onChangeText={handleUsernameChange}
            value={username}
          />
          <Text style={styles.label}>CIN</Text>
          <TextInput
            style={styles.input}
            placeholder="Enter patient CIN"
            onChangeText={handleCinChange}
            value={cin}
            keyboardType="numeric"
          />
          <Text style={styles.label}>Tel</Text>
          <TextInput
            style={styles.input}
            placeholder="Enter patient phone number"
            onChangeText={handleTelChange}
            value={tel}
            keyboardType="phone-pad"
          />
          <Text style={styles.label}>Illnesses</Text>
          <TextInput
            style={styles.input}
            placeholder="Enter patient illnesses"
            onChangeText={handleMaladieChange}
            value={maladie}
          />
          <Text style={styles.label}>Medecines</Text>
          <TextInput
            style={styles.input}
            placeholder="Enter patient medecines"
            onChangeText={handleMedecinesChange}
            value={medecines}
          />
          <Text style={styles.label}>Advices</Text>
          <TextInput
            style={styles.input}
            placeholder="Enter your medical advices"
            onChangeText={handleConseilsChange}
            value={conseils}
          />
          <Button
            title="VALIDATE"
            onPress={handleSubmit}
             />
        </View>
        <View>
          
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#E7E7E7",
  },
  navbar: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 16,
    height: 60,
    backgroundColor: "#E7E7E7",
    marginTop: 10,
  },
  leftContainer: {
    flex: 1,
    alignItems: "flex-start",
  },
  centerContainer: {
    flex: 1,
    alignItems: "center",
  },
  rightContainer: {
    flex: 1,
    alignItems: "flex-end",
  },
  title: {
    fontSize: 23,
    fontWeight: "bold",
    color: Colors.med,
  },
  form: {
    padding: 20,
  },
  label: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 5,
    color : Colors.green ,
  },
  input: {
    backgroundColor: "#fff",
    paddingHorizontal: 10,
    paddingVertical: 8,
    fontSize: 16,
    marginBottom: 20,
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 5,
  },
 
});

export default Ordonnance;