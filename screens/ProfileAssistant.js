import React from "react";
import {
  StyleSheet,
  Dimensions,
  ScrollView,
  Image,
  ImageBackground,
  Platform,
} from "react-native";
import { Block, Text, theme } from "galio-framework";
import Icon from "react-native-vector-icons/FontAwesome"; // Import the Icon component
import { Button } from "../components";
import { Images, argonTheme } from "../constants";
import { HeaderHeight } from "../constants/utils";
import Colors from "../constants/Colors";

const { width, height } = Dimensions.get("screen");

const thumbMeasure = (width - 48 - 32) / 3;

class ProfileAssistant extends React.Component {
  render() {
    return (
      <Block flex style={styles.profile}>
        <Block flex>
          <ImageBackground
            source={Images.ProfileBackground}
            style={styles.profileContainer}
            imageStyle={styles.profileBackground}
          >
            <ScrollView
              showsVerticalScrollIndicator={false}
              style={{ width, marginTop: "25%" }}
            >
              <Block flex style={styles.profileCard}>
                <Block middle style={styles.avatarContainer}>
                  <Image
                    source={require("../assets/imgs/profileA.png")}
                    style={styles.avatar}
                  />
                  <Text bold size={28} color="#32325D" style={styles.name}>
                    Assistant Eya
                  </Text>
                </Block>
                <Block style={styles.info}>
                  <Block
                    middle
                    style={{
                      marginTop: 20,
                      paddingBottom: 120,
                      justifyContent: "center", // Center the buttons horizontally
                    }}
                  >
                    <Block row middle>
                      <Button
                        style={styles.button}
                        textStyle={styles.buttonText}
                      //  onPress={() => this.props.navigation.navigate("Messages", { screen: "Messages",}) }
                      >
                        <Text
                          style={{
                            color: "white",
                            fontWeight: "bold",
                            fontSize: 15,
                          }}
                        >
                          Messages
                        </Text>
                      </Button>
                      <Button
                        style={styles.button}
                        textStyle={styles.buttonText}
                        onPress={() =>
                          this.props.navigation.navigate("NotificationsA", {
                            screen: "NotificationsA",
                          })
                        }
                      >
                        <Text
                          style={{
                            color: "white",
                            fontWeight: "bold",
                            fontSize: 15,
                          }}
                        >
                          Notifications
                        </Text>
                      </Button>
                    </Block>
                    <Block style={styles.line} />
                    <Button
                      style={styles.button1}
                      textStyle={styles.buttonText1}
                      onPress={() =>
                        this.props.navigation.navigate("ChangePassword", {
                          screen: "ChangePassword",
                        })
                      }
                    >
                      <Block row middle>
                        <Icon name="lock" size={25} color="black" />
                        <Text style={[styles.buttonText1, { marginLeft: 15 }]}>
                          Change Password
                        </Text>
                      </Block>
                    </Button>

                    <Button
                      style={styles.button1}
                      textStyle={styles.buttonText1}
                    >
                      <Block row middle>
                        <Icon name="cogs" size={25} color="black" />
                        <Text style={[styles.buttonText1, { marginLeft: 15 }]}>
                          Access settings
                        </Text>
                      </Block>
                    </Button>

                    <Button
                      style={styles.button1}
                      textStyle={styles.buttonText2}
                      onPress={() =>
                        this.props.navigation.navigate("SignIn", {
                          screen: "SignIn",
                        })
                      }
                    >
                      <Block row middle>
                        <Icon name="sign-out" size={25} color="red" />
                        <Text style={[styles.buttonText2, { marginLeft: 15 }]}>
                          Log out
                        </Text>
                      </Block>
                    </Button>
                  </Block>
                </Block>
              </Block>
            </ScrollView>
          </ImageBackground>
        </Block>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  profile: {
    marginTop: Platform.OS === "android" ? -HeaderHeight : 0,
    flex: 1,
  },
  profileContainer: {
    width: width,
    height: height,
    padding: 0,
    zIndex: 1,
  },
  profileBackground: {
    width: width,
    height: height / 2,
  },
  profileCard: {
    padding: theme.SIZES.BASE,
    marginHorizontal: theme.SIZES.BASE,
    marginTop: 65,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    zIndex: 2,
  },
  info: {
    paddingHorizontal: 40,
    justifyContent: "center", // Center the buttons horizontally
  },
  avatarContainer: {
    position: "relative",
    marginTop: -80,
  },
  avatar: {
    width: 124,
    height: 124,
    borderRadius: 62,
    borderWidth: 0,
  },
  name: {
    marginTop: 10,
    textAlign: "center",
  },
  button: {
    width: "55%",
    height: 45,
    backgroundColor: Colors.primary,
    marginRight: -2,
    marginTop: 15,
  },
  buttonText: {
    fontSize: 16,
  },
  button1: {
    width: "145%",
    height: 50,
    backgroundColor: Colors.azure,
    marginTop: 2,
  },
  buttonText1: {
    fontSize: 19,
    color: "#000",
  },
  buttonText2: {
    fontSize: 19,
    color: "red",
  },

  line: {
    width: "120%",
    height: 1,
    backgroundColor: "#E3E3E3",
    marginVertical: 50,
  },
});

export default ProfileAssistant;
