/**
 * The main role of the component is to display the home screen of a user,
 * including features such as a carousel of images, and various buttons
 * for different functions such as appointments, doctors, and medical records.
 */
import React, { useState, useEffect, useRef } from "react";
import {
  Dimensions,
  StyleSheet,
  View,
  TouchableOpacity,
  ImageBackground,
} from "react-native";
import SuiviniButton from "../components/ui/SuiviniButton";
import Colors from "../constants/Colors";
import Carousel, { Pagination } from "react-native-snap-carousel-chen";
import { Image } from "react-native";
import { ScrollView } from "react-native";
import { Animated } from "react-native";
import UserType from "../constants/UserType";
import AppointmentIcon from "../components/svg/AppointmentIcon";
import DoctorsIcon from "../components/svg/DoctorsIcon";
import MedicalRecordsIcon from "../components/svg/MedicalRecordsIcon";
import MedicinesIcon from "../components/svg/MedicinesIcon";
import { Ionicons } from "@expo/vector-icons";
import { Text } from "react-native";
import { BarCodeScanner } from "expo-barcode-scanner";
import { Button } from "react-native";

const Home = (props) => {
  const userType = props.route.params.userType;
  const alertProsition = useRef(new Animated.Value(-60)).current;

  const [activeSlide, setActiveSlide] = useState(0);
  const [carrouselRef, setCarrouselRef] = useState(null);

  const SLIDER_WIDTH = Dimensions.get("window").width - 50;
  const ITEM_WIDTH = Math.round(SLIDER_WIDTH);

  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);

  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === "granted");
      setScanned(true);
    })();
  }, []);

  const handleScan = ({ type, data }) => {
    setScanned(true);
    alert(`Bar code with type ${type} and data ${data} has been scanned!`);
  };

  const handleScanButtonPress = () => {
    setScanned(false);
  };

  if (userType === "medecin") {
     if (hasPermission === null) {
    return <Text>Demande d'autorisation d'accès à la caméra</Text>;
  }
  if (hasPermission === false) {
    return <Text>L'accès à la caméra a été refusé</Text>;
  }

  }

  const CameraPreview = () => (
    <View style={{ flex: 1 }}>
      <BarCodeScanner
        onBarCodeScanned={handleBarCodeScanned}
        style={StyleSheet.absoluteFillObject}
      />
    </View>
  );

  // render image
  const _renderItem = ({ item }) => {
    return (
      <View
        style={{
          borderRadius: 5,
          height: 250,
          width: "100%",
        }}
      >
        <Image
          style={{
            width: ITEM_WIDTH,
            height: 250,
            borderRadius: 5,
            resizeMode: "cover",
          }}
          source={item.image}
        />
      </View>
    );
  };

  if (userType === "patient") {
    return (
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.navbar}>
            <View style={styles.leftContainer}>
              <Ionicons name="menu" size={30} color="black" />
            </View>
            <View style={styles.centerContainer}>
              <Text style={styles.title}>Suivini</Text>
            </View>
            <TouchableOpacity
              style={styles.rightContainer}
              onPress={() =>
                props.navigation.navigate("Profile", { screen: "Profile" })
              }
            >
              <Ionicons name="person-circle" size={30} color="black" />
            </TouchableOpacity>
          </View>
          {/* Images carousel */}
          <View style={{ overflow: "hidden", marginTop: 10 }}>
            <Carousel
              ref={(ref) => {
                setCarrouselRef(ref);
              }}
              data={[
                { image: require("../assets/imgs/slider-1.jpg") },
                { image: require("../assets/imgs/slider-2.jpg") },
                { image: require("../assets/imgs/slider-3.jpg") },
              ]}
              onSnapToItem={(index) => {
                setActiveSlide(index);
              }}
              renderItem={_renderItem}
              sliderWidth={SLIDER_WIDTH}
              itemWidth={ITEM_WIDTH}
              loop={true}
              autoplay={true}
              hasParallaxImages={true}
              firstItem={0}
              inactiveSlideScale={0.94}
              inactiveSlideOpacity={0.5}
              containerCustomStyle={{
                marginTop: 0,
                flexGrow: 0,
                overflow: "visible",
              }}
              contentContainerCustomStyle={{}}
              loopClonesPerSide={1}
              autoplayDelay={500}
              autoplayInterval={3000}
            />
            <Pagination
              dotsLength={3}
              activeDotIndex={activeSlide}
              dotStyle={{
                width: 8,
                height: 8,
                borderRadius: 5,
                backgroundColor: "#E4261B",
              }}
              inactiveDotStyle={{
                width: 8,
                height: 8,
                borderRadius: 5,
                borderWidth: 1,
                borderColor: "#ffffff",
                backgroundColor: "transparent",
              }}
              inactiveDotOpacity={1}
              inactiveDotScale={1}
              carouselRef={carrouselRef}
              tappableDots={!!carrouselRef}
              containerStyle={{
                position: "absolute",
                top: 205,
                width: "100%",
              }}
            />
          </View>
          {/* Display appointments button */}

          <SuiviniButton
            text="Make an Appointment"
            textStyle={styles.buttonTextStyle1}
            style={{
              ...styles.button,
              backgroundColor: "white",
              marginTop: 20,
              borderColor: "#1F82A9",
              borderWidth: 1.5,
            }}
            iconLeft={() => (
              <AppointmentIcon
                style={{ marginRight: 15 }}
                height="31"
                width="50"
                stroke="#1F82A9"
              />
            )}
            iconContainerStyle={{ flex: 1, alignItems: "flex-end" }}
            onPress={() => props.navigation.navigate("Appointments")}
          />
          {/* Display MY APPOINTMENTS button */}
          <Text style={styles.text}>About me:</Text>
          <SuiviniButton
            text="My Appointments"
            textStyle={styles.buttonTextStyle}
            style={{
              ...styles.button,
              backgroundColor: Colors.primary,
              marginTop: 20,
            }}
            iconLeft={() => (
              <AppointmentIcon
                style={{ marginRight: 15 }}
                height="31"
                width="50"
                stroke="#FFF"
              />
            )}
            iconContainerStyle={{ flex: 1, alignItems: "flex-end" }}
            onPress={() => props.navigation.navigate("MyAppointments")}
          />
          {/* Display Doctors button */}
          <SuiviniButton
            text="My Doctors"
            textStyle={styles.buttonTextStyle}
            style={{
              ...styles.button,
              backgroundColor: Colors.primary,
            }}
            iconLeft={() => (
              <DoctorsIcon
                style={{ marginRight: 15 }}
                height="35"
                width="50"
                stroke="#FFF"
              />
            )}
            iconContainerStyle={{ flex: 1, alignItems: "flex-end" }}
            onPress={() =>
              props.navigation.navigate("Doctors", { screen: "Doctors" })
            }
          />
          {/* Display Medical Records button */}
          <SuiviniButton
            text="My Medical Record"
            textStyle={styles.buttonTextStyle}
            style={{
              ...styles.button,
              backgroundColor: Colors.primary,
            }}
            iconLeft={() => (
              <MedicalRecordsIcon
                style={{ marginRight: 15 }}
                height="35"
                width="50"
                stroke="#FFF"
              />
            )}
            iconContainerStyle={{ flex: 1, alignItems: "flex-end" }}
            onPress={() =>
              props.navigation.navigate("MedicalRecords", {
                screen: "MedicalRecords",
              })
            }
          />
          {/* Display Medicines button */}
          <SuiviniButton
            text="My Medicines"
            textStyle={styles.buttonTextStyle}
            style={{ ...styles.button, backgroundColor: Colors.primary }}
            iconLeft={() => (
              <MedicinesIcon
                style={{ marginRight: 20 }}
                height="29"
                width="35"
                stroke="#FFF"
              />
            )}
            iconContainerStyle={{ flex: 1, alignItems: "flex-end" }}
            onPress={() =>
              props.navigation.navigate("Medicines", { screen: "Medicines" })
            }
          />
        </View>
      </ScrollView>
    );
  } else if (userType === "medecin") {
    return (
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.navbar}>
            <View style={styles.leftContainer}>
              <Ionicons name="menu" size={30} color="black" />
            </View>
            <View style={styles.centerContainer}>
              <Text style={styles.title}>Suivini</Text>
            </View>
            <TouchableOpacity
              style={styles.rightContainer}
              onPress={() =>
                props.navigation.navigate("Profile", { screen: "Profile" })
              }
            >
              <Ionicons name="person-circle" size={30} color="black" />
            </TouchableOpacity>
          </View>
          <View style={styles.container}>
            <Text style={styles.titleQR}>
              Scan QR and Follow your Patients
            </Text>
            <View style={styles.containerQR}>
              {!scanned ? (
                <View style={styles.cameraContainer}>
                  <BarCodeScanner
                    onBarCodeScanned={handleScan}
                    style={StyleSheet.absoluteFillObject}
                  />
                </View>
              ) : (
                <View>
                  <ImageBackground
                    source={require("../assets/QR2.png")}
                    style={styles.image}
                  >
                    <View style={styles.qrContainer}>
                      <View style={styles.topContainer}>
                        
                      </View>
                      <View style={styles.bottomContainer}>
                        <Button
                          title={"Scann QR"}
                          onPress={handleScanButtonPress}
                        />
                      </View>
                    </View>
                  </ImageBackground>
                </View>
              )}
            </View>
          </View>
          <SuiviniButton
            text="My patients"
            textStyle={styles.buttonTextStyle}
            style={{ ...styles.button, backgroundColor: Colors.primary }}
            iconLeft={() => (
              <MedicinesIcon
                style={{ marginRight: 20 }}
                height="29"
                width="35"
                stroke="#FFF"
              />
            )}
            iconContainerStyle={{ flex: 1, alignItems: "flex-end" }}
            onPress={() =>
              props.navigation.navigate("Patients", { screen: "Patients" })
            }
          />
      </View>
    </ScrollView>
  );
} else if (userType === "assistant") {
  return (
    <ScrollView>
      <View style={styles.container}>
        <View style={styles.navbar}>
          <View style={styles.leftContainer}>
            <Ionicons name="menu" size={30} color="black" />
          </View>
          <View style={styles.centerContainer}>
            <Text style={styles.title}>Suivini</Text>
          </View>
          <TouchableOpacity
            style={styles.rightContainer}
            onPress={() =>
              props.navigation.navigate("ProfileAssistant", { screen: "ProfileAssistant" })
            }
          >
            <Ionicons name="person-circle" size={30} color="black" />
          </TouchableOpacity>
        </View>
        {/* Images carousel */}
        <View style={{ overflow: "hidden", marginTop: 10 }}>
          <Carousel
            ref={(ref) => {
              setCarrouselRef(ref);
            }}
            data={[
              { image: require("../assets/imgs/slider-1.jpg") },
              { image: require("../assets/imgs/slider-2.jpg") },
              { image: require("../assets/imgs/slider-3.jpg") },
            ]}
            onSnapToItem={(index) => {
              setActiveSlide(index);
            }}
            renderItem={_renderItem}
            sliderWidth={SLIDER_WIDTH}
            itemWidth={ITEM_WIDTH}
            loop={true}
            autoplay={true}
            hasParallaxImages={true}
            firstItem={0}
            inactiveSlideScale={0.94}
            inactiveSlideOpacity={0.5}
            containerCustomStyle={{
              marginTop: 0,
              flexGrow: 0,
              overflow: "visible",
            }}
            contentContainerCustomStyle={{}}
            loopClonesPerSide={1}
            autoplayDelay={500}
            autoplayInterval={3000}
          />
          <Pagination
            dotsLength={3}
            activeDotIndex={activeSlide}
            dotStyle={{
              width: 8,
              height: 8,
              borderRadius: 5,
              backgroundColor: "#E4261B",
            }}
            inactiveDotStyle={{
              width: 8,
              height: 8,
              borderRadius: 5,
              borderWidth: 1,
              borderColor: "#ffffff",
              backgroundColor: "transparent",
            }}
            inactiveDotOpacity={1}
            inactiveDotScale={1}
            carouselRef={carrouselRef}
            tappableDots={!!carrouselRef}
            containerStyle={{
              position: "absolute",
              top: 205,
              width: "100%",
            }}
          />
        </View>
      </View>
      <SuiviniButton
        text="Doctors"
        textStyle={styles.buttonTextStyle}
        style={{
          ...styles.button,
          backgroundColor: Colors.primary,
          marginTop: 20,
        }}
        iconLeft={() => (
          <MedicalRecordsIcon
            style={{ marginRight: 15 }}
            height="35"
            width="50"
            stroke="#FFF"
          />
        )}
        iconContainerStyle={{ flex: 1, alignItems: "flex-end" }}
        // onPress={() =>   props.navigation.navigate("Doctors", { screen: "Doctors" })}
      />
      <SuiviniButton
        text="Appointments"
        textStyle={styles.buttonTextStyle}
        style={{
          ...styles.button,
          backgroundColor: Colors.primary,
          marginTop: 10,
        }}
        iconLeft={() => (
          <AppointmentIcon
            style={{ marginRight: 15 }}
            height="31"
            width="50"
            stroke="#FFF"
          />
        )}
        iconContainerStyle={{ flex: 1, alignItems: "flex-end" }}
        //  onPress={() => props.navigation.navigate("MyAppointments")}
      />
      <SuiviniButton
        text="Patients"
        textStyle={styles.buttonTextStyle}
        style={{
          ...styles.button,
          backgroundColor: Colors.primary,
          marginTop: 10,
        }}
        iconLeft={() => (
          <DoctorsIcon
            style={{ marginRight: 15 }}
            height="31"
            width="50"
            stroke="#FFF"
          />
        )}
        iconContainerStyle={{ flex: 1, alignItems: "flex-end" }}
        //    onPress={() => props.navigation.navigate("MyAppointments")}
      />
    </ScrollView>
  );
}
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#E7E7E7",

    // paddingHorizontal: 20,
  },
  containerQR: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#E7E7E7",
    paddingVertical: 20,
  },
  modalTitle: {
    color: "#fff",
    fontSize: 20,
    justifyContent: "center",
  },
  modalFooter: {
    paddingHorizontal: 30,
    paddingVertical: 20,
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
  },
  text: {
    color: "#fff",
  },
  button: {
    paddingRight: 35,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: 63,
    width: 290,
    marginTop: 10,
    marginLeft: 35,
  },

  buttonTextStyle: {
    fontSize: 17,
    textTransform: "none",
    flex: 2,
    fontWeight: "bold",
  },
  buttonTextStyle1: {
    fontSize: 17,
    textTransform: "none",
    flex: 2.1,
    fontWeight: "bold",
    color: Colors.primary,
  },

  buttonStyle: {
    fontSize: 15,
    textTransform: "none",
  },
  navbar: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 16,
    height: 60,
    backgroundColor: "#E7E7E7",
    marginTop: 10,
  },
  leftContainer: {},

  centerContainer: {
    flex: 1,
    alignItems: "center",
  },
  text: {
    fontSize: 17,
    marginLeft: 15,
    marginTop: 15,
    width: 300,
    fontWeight: "bold",
  },
  title: {
    fontSize: 23,
    fontWeight: "bold",
    color: Colors.med,
  },
  titleQR: {
    flex: 1,
    marginTop: 25,
    fontSize: 23,
    fontWeight: "bold",
  //  color: Colors.med,
    alignItems: "center",
    justifyContent: "center",
    maxWidth:"90%",
    textAlign: "center"
  },
  rightContainer: {},
  buttonCamera: {
    backgroundColor: "#007AFF",
    borderRadius: 10,
    padding: 16,
    flexDirection: "row",
    alignItems: "center",
  },
  maintext: {
    fontSize: 16,
    margin: 20,
  },
  barcodebox: {
    alignItems: "center",
    justifyContent: "center",
    height: 300,
    width: 300,
    overflow: "hidden",
    borderRadius: 30,
    backgroundColor: "tomato",
  },
  cameraContainer: {
    height: 400,
    width: 400,
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
    flexDirection: "row",
    height: 300,
    width: 300,
  },
  qrContainer: {
    flex: 1,
    justifyContent: "space-between"
  },
  topContainer: {
    alignItems: "center",
    paddingTop: 10,
  },
  bottomContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: 63,
    width: 290,
  },
 
});

export default Home;
