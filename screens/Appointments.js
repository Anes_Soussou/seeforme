import React from "react";
import {
  Text,
  View,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import Colors from "../constants/Colors";
import { SafeAreaView } from "react-native-safe-area-context";

const SpecialityButton = ({ icon, label, onPress  }) => {
  return (
    <TouchableOpacity style={styles.specialityButton} onPress={onPress}>
      <Ionicons name={icon} size={24} color="#FFF" />
      <Text style={styles.specialityLabel}>{label}</Text>
    </TouchableOpacity>
  );
};

const Appointments = function (props) {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.navbar}>
        <View style={styles.leftContainer}>
          <Ionicons name="menu" size={30} color="black" />
        </View>
        <View style={styles.centerContainer}>
          <Text style={styles.title}>Suivini</Text>
        </View>
        <View style={styles.rightContainer}>
          <Ionicons name="person-circle" size={30} color="black" />
        </View>
      </View>
      <View style={styles.contentContainer}>
        <Text style={styles.specialityText}>Choose a speciality</Text>
        <View style={styles.buttonContainer}>
          <SpecialityButton
            icon="medkit-outline"
            label="Generalist"
            onPress={() =>
              props.navigation.navigate("DoctorsInfoScreen", {
                screen: "DoctorsInfoScreen",
              })
            }
          />
          <SpecialityButton icon="flame-outline" label="Endocrinologist" />
        </View>
        <View style={styles.buttonContainer}>
          <SpecialityButton icon="md-pulse" label="Hematologist" />
          <SpecialityButton icon="eye-outline" label="Ophthalmologist" />
        </View>
        <View style={styles.buttonContainer}>
          <SpecialityButton icon="female-outline" label="Gynaecologist" />
          <SpecialityButton icon="male-outline" label="Urologist" />
        </View>
        <View style={styles.buttonContainer}>
          <SpecialityButton icon="heart-outline" label="Cardiologist" />
          <SpecialityButton icon="ear-outline" label="ENT" />
        </View>
        <View style={styles.buttonContainer}>
          <SpecialityButton icon="bandage-outline" label="Orthopedist" />
          <SpecialityButton icon="md-body-outline" label="Gastro" />
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#E7E7E7",
  },
  navbar: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 16,
    height: 60,
    backgroundColor: "#E7E7E7",
    marginTop: 10,
  },
  leftContainer: {
    flex: 1,
    alignItems: "flex-start",
  },
  centerContainer: {
    flex: 1,
    alignItems: "center",
  },
  rightContainer: {
    flex: 1,
    alignItems: "flex-end",
  },
  title: {
    fontSize: 23,
    fontWeight: "bold",
    color: Colors.med,
  },
  contentContainer: {
    flex: 1,
    padding: 16,
    marginTop: 25,
  },
  specialityText: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 10,
    marginLeft: 7,
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 10,
  },

  specialityButton: {
    alignItems: "center",
    backgroundColor: Colors.primary,
    borderRadius: 8,
    padding: 10,
    width: 140,
  },
  specialityLabel: {
    color: "white",
    fontSize: 14,
    marginTop: 5,
  },
});

export default Appointments;
