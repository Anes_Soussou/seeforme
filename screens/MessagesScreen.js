import React, { useState } from "react";
import {
  Text,
  View,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
} from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { Ionicons } from "@expo/vector-icons";

const MessagesScreen = function (props) {
  const discussions = [
    {
      id: "1",
      profileImage: require("../assets/imgs/doctor.png"),
      message: "Dr. John Doe sent you a message",
    },
    {
      id: "2",
      profileImage: require("../assets/imgs/doctore.png"),
      message: "Dr. Khalfallah Mayssa sent you a message ",
    },
    {
      id: "3",
      profileImage: require("../assets/imgs/doctor.png"),
      message: "Dr. Ben rejeb Oussama sent you a message",
    },
  ];

  const [selectedIds, setSelectedIds] = useState([]);

  const renderDiscussion = ({ item }) => {
  const isSelected = selectedIds.includes(item.id);
  return (
    <TouchableOpacity
      style={[
        styles.discussionContainer,
        isSelected && styles.selectedDiscussion,
      ]}
      // onPress={() => toggleDiscussion(item.id)}
      activeOpacity={0.8}
      onPress={()=> props.navigation.navigate('Conversation')}
    >
      <View style={styles.profileContainer}>
        <Image source={item.profileImage} style={styles.profileImage} />
      </View>
      <View style={styles.messageContainer}>
        <Text style={styles.messageText}>{item.message}</Text>
      </View>
    </TouchableOpacity>
  );
};


  const toggleDiscussion = (id) => {
    if (selectedIds.includes(id)) {
      setSelectedIds(selectedIds.filter((item) => item !== id));
    } else {
      setSelectedIds([...selectedIds, id]);
    }
  };


  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.navbar}>
        <Ionicons name="md-mail" size={30} style={styles.icon} />
        <Text style={styles.title}>My messages</Text>
      </View>
      <FlatList
        data={discussions}
        renderItem={renderDiscussion}
        keyExtractor={(item) => item.id}
        contentContainerStyle={styles.discussionList}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#E7E7E7",
  },
  navbar: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    paddingHorizontal: 16,
    height: 80,
    backgroundColor: "#fff",
  },
  icon: {
    marginRight: 10,
    marginLeft: 10,
    color: "#FFD700",
    marginTop: 15,
  },
  title: {
    fontSize: 22,
    fontWeight: "bold",
    marginTop: 15,
  },
  discussionList: {
    flexGrow: 1,
  },
  discussionContainer: {
    flexDirection: "row",
    paddingHorizontal: 16,
    paddingVertical: 15,
    backgroundColor: "#fff",
    marginTop: 1,
    //   borderRadius: 20,
    height: 90,
    alignItems: "center",
  },
  selectedDiscussion: {
    backgroundColor: "#FFFFFF",
  },
  profileContainer: {
    justifyContent: "center",
    marginRight: 10,
  },
  profileImage: {
    width: 48,
    height: 48,
    borderRadius: 24,
  },
  messageContainer: {
    flex: 1,
    padding: 10,
  },
  messageText: {
    fontSize: 16,
  },
});

export default MessagesScreen;
