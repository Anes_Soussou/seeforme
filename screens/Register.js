import React, { useState } from "react";
import {
  StyleSheet,
  ImageBackground,
  Dimensions,
  StatusBar,
  KeyboardAvoidingView,
  ScrollView,
} from "react-native";
import { Block, Checkbox, Text, theme } from "galio-framework";

import { Button, Icon, Input } from "../components";
import { argonTheme } from "../constants";
import Colors from "../constants/Colors";
import SuiviniInput from "../components/ui/SuiviniInput";

const { width, height } = Dimensions.get("screen");

const Register = function (props) {
  const initErrors = {
    nomError: "",
    emailError: "",
    passwordError: "",
    passwordConfirmationError: "",
  };

  const [nom, setNom] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirmation, setPasswordConfirmation] = useState("");
  const [errors, setErrors] = useState(initErrors);

  const next = async () => {
    // reset errors
    setErrors(initErrors);
    // validation
    const valid = await validation();
    if (valid) {
      props.navigation.navigate("Home", {});
    }
    console.log("valid", initErrors);
  };

  const handleSubmitEditing = () => {
    next();
  };

  const validEmailRegex = RegExp(
    /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
  );

  const validation = async () => {
    let valid = true;
    if (!nom) {
      valid = false;
      setErrors((errors) => ({ ...errors, nomError: "Required fields" }));
    }
    if (!email) {
      valid = false;
      setErrors((errors) => ({ ...errors, emailError: "Required fields" }));
    }

    const emailValid = validEmailRegex.test(email.trim());
    // if (emailValid) {
    //   const response = await props.verifyEmail({ email });
    //   if (!(response instanceof HttpErrorResponseModel)) {
    //     if (response.exist) {
    //       valid = false;
    //       setErrors((errors) => ({
    //         ...errors,
    //         emailError: 'Cette adresse e-mail est déjà utilisée',
    //       }));
    //     }
    //   }
    // } else {
    if (!emailValid) {
      valid = false;
      setErrors((errors) => ({
        ...errors,
        emailError: "Please enter a valid email address",
      }));
    }

    if (!password) {
      valid = false;
      setErrors((errors) => ({
        ...errors,
        passwordError: "Required fields",
      }));
    }
    if (password) {
      if (password.length < 6) {
        setErrors((errors) => ({
          ...errors,
          passwordError: "The password must be at least 6 characters long",
        }));
        valid = false;
      } else if (password !== passwordConfirmation) {
        setErrors((errors) => ({
          ...errors,
          passwordConfirmationError:
            "Your password does not match your confirmation password",
        }));
        valid = false;
      }
    }
    if (!passwordConfirmation) {
      valid = false;
      setErrors((errors) => ({
        ...errors,
        passwordConfirmationError: "Champs obligatoire",
      }));
    }
    return valid;
  };

  return (
    <Block flex middle style={{ backgroundColor: "#fff" }}>
      <ScrollView contentContainerStyle={{ alignItems: "center", flex: 1 }}>
        <StatusBar hidden />

        <Block safe flex middle>
          <Block style={styles.registerContainer}>
            <Block flex={0.24} middle style={styles.socialConnect}>
              <Text color="#000" size={13} marginTop={10}>
                Sign up with
              </Text>
              <Block row marginTop={10}>
                <Button style={styles.socialButtons}>
                  <Block row>
                    <Icon
                      name="logo-google"
                      family="Ionicon"
                      size={14}
                      color={"#000"}
                      style={{ marginTop: 2, marginRight: 5 }}
                    />
                    <Text style={styles.socialTextButtons}>GOOGLE</Text>
                  </Block>
                </Button>
              </Block>
            </Block>
            <Block flex>
              <Block center>
                <Text color="#000" size={13} marginTop={14}>
                  Or sign up the classic way
                </Text>
              </Block>
              <Block center>
                <KeyboardAvoidingView
                  style={{ flex: 1 }}
                  behavior="padding"
                  enabled
                >
                  <Block width={width * 0.8}>
                    <SuiviniInput
                      placeholder={"Name"}
                      value={nom}
                      autoCapitalize="characters"
                      onChangeText={(value) => {
                        setNom(value);
                        setErrors((errors) => ({ ...errors, nomError: "" }));
                      }}
                      onSubmitEditing={handleSubmitEditing}
                      errorMessage={errors.nomError}
                      containerStyle={{ width: "100%" }}
                      inputContainerStyle={{ width: "100%" }}
                      iconContent={
                        <Icon
                          size={16}
                          color={argonTheme.COLORS.ICON}
                          name="hat-3"
                          family="ArgonExtra"
                          style={styles.inputIcons}
                        />
                      }
                    />
                  </Block>
                  <Block width={width * 0.8}>
                    <SuiviniInput
                      borderless
                      placeholder="Email"
                      value={email}
                      onChangeText={(value) => {
                        setEmail(value);
                        setErrors((errors) => ({ ...errors, emailError: "" }));
                      }}
                      onSubmitEditing={handleSubmitEditing}
                      errorMessage={errors.emailError}
                      autoCapitalize="none"
                      iconContent={
                        <Icon
                          size={16}
                          color={argonTheme.COLORS.ICON}
                          name="ic_mail_24px"
                          family="ArgonExtra"
                          style={styles.inputIcons}
                        />
                      }
                    />
                  </Block>
                  <Block width={width * 0.8}>
                    <SuiviniInput
                      borderless
                      placeholder="Password"
                      value={password}
                      onChangeText={(value) => {
                        setPassword(value.trim());
                        setErrors((errors) => ({
                          ...errors,
                          passwordError: "",
                        }));
                      }}
                      onSubmitEditing={handleSubmitEditing}
                      secureTextEntry={true}
                      errorMessage={errors.passwordError}
                      iconContent={
                        <Icon
                          size={16}
                          color={argonTheme.COLORS.ICON}
                          name="padlock-unlocked"
                          family="ArgonExtra"
                          style={styles.inputIcons}
                        />
                      }
                    />
                    <SuiviniInput
                      borderless
                      placeholder="Confirm your password"
                      value={passwordConfirmation}
                      onChangeText={(value) => {
                        setPasswordConfirmation(value.trim());
                        setErrors((errors) => ({
                          ...errors,
                          passwordConfirmationError: "",
                        }));
                      }}
                      onSubmitEditing={handleSubmitEditing}
                      secureTextEntry={true}
                      errorMessage={errors.passwordConfirmationError}
                      iconContent={
                        <Icon
                          size={16}
                          color={argonTheme.COLORS.ICON}
                          name="padlock-unlocked"
                          family="ArgonExtra"
                          style={styles.inputIcons}
                        />
                      }
                    />
                  </Block>
                  <Block row width={width * 0.75}>
                    <Checkbox
                      checkboxStyle={{
                        borderWidth: 3,
                      }}
                      color={argonTheme.COLORS.PRIMARY}
                      label="I agree with the"
                    />
                    <Button
                      style={{ width: 100 }}
                      color="transparent"
                      textStyle={{
                        color: argonTheme.COLORS.PRIMARY,
                        fontSize: 14,
                      }}
                    >
                      Privacy Policy
                    </Button>
                  </Block>
                  <Block middle>
                    <Button
                      onPress={() => {
                        next();
                      }}
                      style={styles.createButton}
                    >
                      <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                        CREATE ACCOUNT
                      </Text>
                    </Button>
                  </Block>
                </KeyboardAvoidingView>
              </Block>
            </Block>
          </Block>
        </Block>
      </ScrollView>
    </Block>
  );
};
const styles = StyleSheet.create({
  registerContainer: {
    width: width * 0.9,
    height: height * 0.9,
    backgroundColor: Colors.azure,
    borderRadius: 4,
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1,
    overflow: "hidden",
  },
  socialConnect: {
    backgroundColor: Colors.azure,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: "#8898AA",
  },
  socialButtons: {
    width: width * 0.5,
    // flex: 1,
    height: 40,
    backgroundColor: argonTheme.COLORS.PRIMARY,
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1,
  },
  socialTextButtons: {
    color: "#fff",
    fontWeight: "800",
    fontSize: 14,
  },
  inputIcons: {
    marginRight: 12,
  },
  passwordCheck: {
    paddingLeft: 15,
    paddingTop: 13,
    paddingBottom: 30,
  },
  createButton: {
    width: width * 0.5,
    marginTop: 25,
  },
});

export default Register;
