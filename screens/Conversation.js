import React, { useState } from "react";
import {
  Text,
  View,
  ScrollView,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import Colors from "../constants/Colors";
import { SafeAreaView } from "react-native-safe-area-context";

const Conversation = function (props) {
  const [newMessage, setNewMessage] = useState("");
  const [messages, setMessages] = useState([
    { text: "Hello, how can I help you?", sender: "doctor" },
    {
      text: "I'm not feeling well, I have a headache and a fever",
      sender: "patient",
    },
    { text: "Okay, have you taken any medication?", sender: "doctor" },
  ]);

  const onSendMessage = () => {
    if (newMessage.trim() === "") {
      return;
    }
    const newMessageObj = { text: newMessage, sender: "patient" };
    setMessages([...messages, newMessageObj]);
    setNewMessage("");
  };

  const renderMessage = (message, index) => {
    const messageStyle =
      message.sender === "doctor"
        ? styles.doctorMessage
        : styles.patientMessage;
    const containerStyle =
      message.sender === "doctor"
        ? styles.doctorContainer
        : styles.patientContainer;

    return (
      <View key={index} style={[styles.messageContainer, containerStyle]}>
        <View style={[styles.messageBubble, messageStyle]}>
          <Text style={styles.messageText}>{message.text}</Text>
        </View>
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.navbar}>
        <View style={styles.leftContainer}>
          <Ionicons name="menu" size={30} color="black" />
        </View>
        <View style={styles.centerContainer}>
          <Text style={styles.title}>Suivini</Text>
        </View>
        <View style={styles.rightContainer}>
          <Ionicons name="person-circle" size={30} color="black" />
        </View>
      </View>
      <ScrollView
        style={styles.contentContainer}
        contentContainerStyle={styles.messagesContainer}
        ref={(ref) => {
          this.scrollView = ref;
        }}
        onContentSizeChange={() => {
          this.scrollView.scrollToEnd({ animated: true });
        }}
      >
        {messages.map((message, index) => renderMessage(message, index))}
      </ScrollView>
      <View style={styles.newMessageContainer}>
        <TextInput
          style={styles.newMessageInput}
          placeholder="Type your message here"
          onChangeText={setNewMessage}
          value={newMessage}
        />
        <TouchableOpacity style={styles.sendButton} onPress={onSendMessage}>
          <Ionicons name="send" size={24} color="white" />
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#E7E7E7",
  },
  navbar: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 16,
    height: 60,
    backgroundColor: "#E7E7E7",
    marginTop: 10,
  },
  contentContainer: {
    flex: 1,
    paddingTop: 20,
    paddingHorizontal: 16,
  },
  messagesContainer: {
    paddingBottom: 60,
  },
  newMessageContainer: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 16,
    paddingVertical: 8,
    backgroundColor: "#FFFFFF",
    borderTopWidth: 1,
    borderTopColor: "#E7E7E7",
  },
  newMessageInput: {
    flex: 1,
    height: 50,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: "#E7E7E7",
    borderRadius: 20,
    marginRight: 10,
  },
  sendButton: {
    backgroundColor: Colors.primary,
    width: 40,
    height: 40,
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
  },
  leftContainer: {
    flex: 1,
    alignItems: "flex-start",
  },
  centerContainer: {
    flex: 1,
    alignItems: "center",
  },
  rightContainer: {
    flex: 1,
    alignItems: "flex-end",
  },
  title: {
    fontSize: 23,
    fontWeight: "bold",
    color: Colors.med,
  },
  messageContainer: {
    marginBottom: 10,
    paddingHorizontal: 10,
  },
  doctorContainer: {
    alignSelf: "flex-start",
    backgroundColor: "#8AD3F2",
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
  patientContainer: {
    alignSelf: "flex-end",
    backgroundColor: "#57B8FF",
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
  },
  messageBubble: {
    padding: 10,
  },
  doctorMessage: {
    backgroundColor: "#8AD3F2", // Light blue
  },
  patientMessage: {
    backgroundColor: "#57B8FF", // Pale blue
  },

  messageText: {
    fontSize: 16,
    color: "#FFFFFF",
  },
});


export default Conversation;
