import React from "react";
import { TouchableOpacity } from "react-native";
import { StyleSheet, Text, View, Image } from "react-native";
import Colors from "../constants/Colors";
import { Alert } from "react-native";
import SuiviniButton from "../components/ui/SuiviniButton";
import { Ionicons } from "@expo/vector-icons";

const Decision = function (props) {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: "#fff",
        paddingHorizontal: 5,
        paddingVertical: 5,
      }}
    >
      <View style={{ flex: 1 }}>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            marginVertical: 5,
            paddingBottom: 10,
            borderBottomWidth: 1,
            borderColor: Colors.grey,
          }}
        >
          <Image
            source={require("../assets/imgs/woman.png")}
            style={{
              width: 55,
              height: 55,
              borderRadius: 40,
              marginTop: 15,
              borderWidth: 2,
              borderColor: "#fff",
            }}
          />

          <View style={{ flexDirection: "column", flex: 2 }}>
            <Text
              style={{
                color: Colors.black,
                marginLeft: 5,
                fontSize: 20,
                marginTop: 15,
              }}
              numberOfLines={3}
              ellipsizeMode="tail"
            >
              Guirat Chayma
            </Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginTop: 15,
              marginRight: 10,
            }}
          >
            <Ionicons
              name="md-person-add"
              size={23}
              color={Colors.grey}
              style={{ marginRight: 25 }}
            />
            <Ionicons name="md-chatbubbles" size={23} color={Colors.grey} />
          </View>
        </View>

        <View style={styles.menuWrapper}>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Text
              style={{
                paddingHorizontal: 10,
                fontSize: 18,
                color: "#28777A",
                fontWeight: "bold",
              }}
            >
              Doctor :
            </Text>

            <TouchableOpacity style={[styles.valueAdress]}>
              <Text style={styles.valueText} numberOfLines={2}>
                Dr. John Doe{" "}
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Text
              style={{
                paddingHorizontal: 10,
                fontSize: 18,
                color: "#28777A",
                fontWeight: "bold",
              }}
            >
              Date :
            </Text>

            <TouchableOpacity style={[styles.valueAdress]}>
              <Text style={styles.valueText} numberOfLines={2}>
                08/05/2023{" "}
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Text
              style={{
                paddingHorizontal: 10,
                fontSize: 18,
                color: "#28777A",
                fontWeight: "bold",
              }}
            >
              Hour :
            </Text>

            <TouchableOpacity style={[styles.valueAdress]}>
              <Text style={styles.valueText} numberOfLines={2}>
                03.00 pm{" "}
              </Text>
            </TouchableOpacity>
          </View>

          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Text style={styles.labelText}>Description: </Text>
          </View>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Text style={styles.valueTextDegree}>
              I have severe itching that I can no longer sleep and it bothers me
              a lot
            </Text>
          </View>
          <View style={styles.view}>
            <View style={styles.buttonContainer}>
              <SuiviniButton
                text="Accept"
                textStyle={styles.buttonTextStyle}
                style={{
                  ...styles.button,
                  backgroundColor: Colors.med,
                }}
                onPress={() => {
                  Alert.alert("Success", "The Appointment is accepted.");
                }}
              />
            </View>
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginVertical: 15,
              }}
            >
              <View
                style={{ flex: 1, height: 1, backgroundColor: Colors.grey }}
              />
              <Text style={{ marginHorizontal: 10, fontSize: 16 }}>OR</Text>
              <View
                style={{ flex: 1, height: 1, backgroundColor: Colors.grey }}
              />
            </View>

            <View style={styles.buttonContainer}>
              <SuiviniButton
                text="Reject"
                textStyle={styles.buttonTextStyle}
                style={{
                  ...styles.button,
                  backgroundColor: "#B71C1C",
                }}
                onPress={() => {
                  Alert.alert("Success", "The Appointment is rejected.");
                }}
              />
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  // Modal
  modal: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: Colors.overlay,
  },

  buttonText: { textTransform: "none", flex: 1, textAlign: "center" },
  // menu
  menuWrapper: {
    marginHorizontal: 5,
    flex: 1,
  },
  container: {
    flex: 1,
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    // paddingTop: 10,
    //  marginTop: 10,
  },
  value: {
    height: 40,
    justifyContent: "center",
    backgroundColor: Colors.background,
    paddingHorizontal: 5,
  },
  valueAdress: {
    height: 45,
    width: 305,
    borderRadius: 10,
    justifyContent: "center",
    backgroundColor: Colors.background,
    flex: 1,
    marginVertical: 5,
  },
  valueText: {
    fontSize: 15,
    color: "#515151",
    fontWeight: "bold",
    marginLeft: 20,
  },
  labelText: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 12,
    color: Colors.green,
    paddingHorizontal: 10,
  },
  valueTextDegree: {
    fontSize: 17,
    marginTop: 10,
    marginLeft: 13,
    lineHeight: 22,
  },
  button: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: 55,
    width: 200,
    //  marginTop: 50,
    alignSelf: "center",
  },
  buttonTextStyle: {
    fontSize: 18,
    textTransform: "none",
    fontWeight: "bold",
    letterSpacing: 2,
  },
  view: {
    marginTop: 70,
   
  },
});
export default Decision;
