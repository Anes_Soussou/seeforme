import React from "react";
import {
  ImageBackground,
  Image,
  StyleSheet,
  StatusBar,
  Dimensions
} from "react-native";
import { Block, Button, Text, theme } from "galio-framework";

const { height, width } = Dimensions.get("screen");

import argonTheme from "../constants/Theme";
import Images from "../constants/Images";
import Colors from "../constants/Colors";

class Onboarding extends React.Component {
  render() {
    const { navigation } = this.props;

    return (
      <Block flex style={styles.container}>
        <StatusBar hidden />
        <Block flex center>
        <ImageBackground
            source={Images.Onboarding}
            style={{ height, width, zIndex: 1  }}
            //backgroundColor={Colors.azure}
          />
        </Block>
       <Block>
          <Block style={styles.header}>
            <Block>
              <Text color="white" size={60}>
                Suivini
              </Text>
            </Block>
          </Block>
        </Block>
        <Block flex space="between" style={styles.padded}>
          <Block flex space="around" style={{ zIndex: 2 }}>
            <Block style={styles.title}>
              <Block>
                <Text color="white" size={37}>
                  Every
                </Text>
              </Block>
              <Block>
                <Text color="white" size={45}>
                  Moment
                </Text>
              </Block>
              <Block style={styles.subTitle}>
                <Text color="white" size={16}>
                  24/24
                </Text>
              </Block>
            </Block>
            <Block center>
              <Button
                style={styles.button}
                color={argonTheme.COLORS.SECONDARY}
                onPress={() => navigation.navigate("SignIn")}
                textStyle={{ color: argonTheme.COLORS.BLACK }}
              >
                Welcome
              </Button>
            </Block>
          </Block>
        </Block>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.COLORS.BLACK,
  },
  padded: {
    paddingHorizontal: theme.SIZES.BASE * 2,
    position: "relative",
    bottom: theme.SIZES.BASE,
    zIndex: 2,
  },
  button: {
    width: width - theme.SIZES.BASE * 4,
    height: theme.SIZES.BASE * 3,
    shadowRadius: 0,
    shadowOpacity: 0,
  },
  header: {
    paddingHorizontal: theme.SIZES.BASE * 2,
    zIndex: 2,
    position: "relative",
    marginTop: "-55%",

  },
  title: {
    marginTop: 10,
  },
  subTitle: {
    marginTop: 15,
  },
});

export default Onboarding;

