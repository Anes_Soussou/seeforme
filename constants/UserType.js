export default UserTypes = {
    SUPER_ADMIN: 'user.super-admin',
    DOCTOR: 'user.doctor',
    PATIENT: 'user.patient',
    ASSISTANT: 'user.assistant',
  };